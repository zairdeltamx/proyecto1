<?php

namespace App\Http\Controllers;

use App\Models\Tb_gas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Illuminate\Support\Facades\Http;
class TbGasController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function historial(){

        return Tb_gas::all();

    }
    public function sacar(){

        $data = Tb_gas::latest('id')->first();
        return $data;

    }
    public function index(){

        $respuesta=HTTP::get('https://io.adafruit.com/api/v2/zairdeltamx/feeds/gas/data/last?X-AIO-Key=aio_biTe50xUkfFJL4M8Xscp1haHaScs');
        $data=$respuesta->object();
            $model = new Tb_gas();
        
            $model->gas=$data->value;
           
            $model->save();


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tb_gas::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'gas guardado'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tb_gas $Tb_gas)
    {
        return response()->json([
            'res'=>true,
            'gas'=>$Tb_gas
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tb_gas $Tb_gas)
    {
        $Tb_gas->update($Tb_gas->all());
        return response()->json([
            'res'=>true,
            'msg'=>'gas actualizado'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tb_gas $Tb_gas)
    {
        $Tb_gas->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'gas eliminado'
        ],200);
    }
}
