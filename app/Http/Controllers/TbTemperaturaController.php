<?php

namespace App\Http\Controllers;

use App\Models\Tb_temperatura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Illuminate\Support\Facades\Http;

class TbTemperaturaController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function historial(){

        return Tb_temperatura::all();

    }
    public function sacar(){

        $data = Tb_temperatura::latest('id')->first();
        return $data;

    }
    public function index(){

        $respuesta=HTTP::get('https://io.adafruit.com/api/v2/zairdeltamx/feeds/temperatura/data/last?X-AIO-Key=aio_biTe50xUkfFJL4M8Xscp1haHaScs');
        $data=$respuesta->object();
            $model = new Tb_temperatura();
        
            $model->temperatura=$data->value;
           
            $model->save();


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tb_temperatura::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'temp guardado'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tb_temperatura $Tb_temperatura)
    {
        return response()->json([
            'res'=>true,
            'temp'=>$Tb_temperatura
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tb_temperatura $Tb_temperatura)
    {
        $Tb_temperatura->update($Tb_temperatura->all());
        return response()->json([
            'res'=>true,
            'msg'=>'temp actualizado'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tb_temperatura $Tb_temperatura)
    {
        $Tb_temperatura->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'temp eliminado'
        ],200);
    }
}
