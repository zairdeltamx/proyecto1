<?php

namespace App\Http\Controllers;

use App\Models\Tb_movimiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Illuminate\Support\Facades\Http;
class TbMovimientoController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function historial(){

        return Tb_movimiento::all();

    }
    public function sacar(){

        $data = Tb_movimiento::latest('id')->first();
        return $data;

    }
    public function index(){

        $respuesta=HTTP::get('https://io.adafruit.com/api/v2/zairdeltamx/feeds/movimiento/data/last?X-AIO-Key=aio_biTe50xUkfFJL4M8Xscp1haHaScs');
        $data=$respuesta->object();
            $model = new Tb_movimiento();
        
            $model->movimiento=$data->value;
           
            $model->save();


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tb_movimiento::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'movimiento guardado'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tb_movimiento $Tb_movimiento)
    {
        return response()->json([
            'res'=>true,
            'movimiento'=>$Tb_movimiento
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tb_movimiento $Tb_movimiento)
    {
        $Tb_movimiento->update($Tb_movimiento->all());
        return response()->json([
            'res'=>true,
            'msg'=>'movimiento actualizado'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tb_movimiento $Tb_movimiento)
    {
        $Tb_movimiento->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'movimiento eliminado'
        ],200);
    }
}
