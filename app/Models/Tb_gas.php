<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Tb_gas extends Model
{
    protected $fillable = [
        'gas'
    ];
protected $primarykey='id';
    public function getJWTIdentifier()
    {
    	return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
    	return [];
    }
}
