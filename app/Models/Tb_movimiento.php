<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Tb_movimiento extends Model
{
    protected $table ="tb_movimientos";
     
    protected $fillable = [
    
        'movimiento',
        
    ];

    protected $primarykey='id';
    public function getJWTIdentifier()
    {
    	return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
    	return [];
    }
}
