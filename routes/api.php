<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use app\Htpp\Controllers\UserController;
use app\Htpp\Controllers\TbTemperaturaController;
use app\Htpp\Controllers\TbHumedadController;
use app\Htpp\Controllers\TbGasController;
use app\Htpp\Controllers\TbMovimientoController;
use app\Htpp\Controllers\TbTarjetaController;

use app\Htpp\Controllers\TbLedController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
  //  return $request->user();
//});

Route::post('register', 'App\Http\Controllers\UserController@register');
Route::post('login', 'App\Http\Controllers\UserController@authenticate');

Route::group(['middleware' => ['jwt.verify']], function() {

    Route::post('user','App\Http\Controllers\UserController@getAuthenticatedUser');
   

    Route::get('ultimotemperatura','App\Http\Controllers\TbTemperaturaController@sacar');
    Route::get('historialtemperatura','App\Http\Controllers\TbTemperaturaController@historial');
    Route::get('guardartemperatura','App\Http\Controllers\TbTemperaturaController@index');
    Route::post('Temperatura',[TbTemperaturaController::class,'store']);
    Route::get('Temperatura/{id}',[TbTemperaturaController::class,'show']);
    Route::put('Temperatura/{id}',[TbTemperaturaController::class,'update']);
    Route::delete('Temperatura/{id}',[TbTemperaturaController::class,'destroy']);

    Route::get('ultimohumedad','App\Http\Controllers\TbHumedadController@sacar');
    Route::get('historialhumedad','App\Http\Controllers\TbHumedadController@historial');
    Route::get('guardarhumedad','App\Http\Controllers\TbHumedadController@index');
    Route::post('Humedad',[TbHumedadController::class,'store']);
    Route::get('Humedad/{id}',[TbHumedadController::class,'show']);
    Route::put('Humedad/{id}',[TbHumedadController::class,'update']);
    Route::delete('Humedad/{id}',[TbHumedadController::class,'destroy']);
    
    Route::get('guardargas','App\Http\Controllers\TbGasController@index');
    Route::get('ultimogas','App\Http\Controllers\TbGasController@sacar');
    Route::get('historialgas','App\Http\Controllers\TbGasController@historial');
    Route::post('Gas',[TbGasController::class,'store']);
    Route::get('Gas/{id}',[TbGasController::class,'show']);
    Route::put('Gas/{id}',[TbGasController::class,'update']);
    Route::delete('Gas/{id}',[TbGasController::class,'destroy']);


    Route::get('ultimomovimiento','App\Http\Controllers\TbMovimientoController@sacar');
    Route::get('historialmovimiento','App\Http\Controllers\TbMovimientoController@historial');
    Route::get('Movimiento','App\Http\Controllers\TbMovimientoController@index');
    Route::post('Movimiento',[TbMovimientoController::class,'store']);
    Route::get('Movimiento/{id}',[TbMovimientoController::class,'show']);
    Route::put('Movimiento/{id}',[TbMovimientoController::class,'update']);
    Route::delete('Movimiento/{id}',[TbMovimientoController::class,'destroy']);

    Route::get('ultimonfc','App\Http\Controllers\TbTarjetaController@sacar');
    Route::get('historialnfc','App\Http\Controllers\TbTarjetaController@historial');
    Route::get('guardarnfc','App\Http\Controllers\TbTarjetaController@index');
    Route::post('Nfc',[TbTarjetaController::class,'store']);
    Route::get('Nfc/{id}',[TbTarjetaController::class,'show']);
    Route::put('Nfc/{id}',[TbTarjetaController::class,'update']);
    Route::delete('Nfc/{id}',[TbTarjetaController::class,'destroy']);


});

    